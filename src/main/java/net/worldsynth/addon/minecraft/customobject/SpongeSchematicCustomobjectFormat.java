/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.customobject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntArrayTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ShortTag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile.FlattenedLookup;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.customobject.Block;
import net.worldsynth.customobject.CustomObject;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialRegistry;
import net.worldsynth.material.Property;
import net.worldsynth.material.StateProperties;

// Sponge schematic specification
// https://github.com/SpongePowered/Schematic-Specification
public class SpongeSchematicCustomobjectFormat extends CustomObjectFormat {
	
	@Override
	public CustomObject readObjectFromFile(File file) throws IOException {
		CompoundTag schematicCompound = NBTIO.readFile(file);
		
		int version = ((IntTag) schematicCompound.get(TAG_VERSION)).getValue();
		
		switch (version) {
		case 1:
			return parseObjectV1(schematicCompound);
		case 2:
			return parseObjectV2(schematicCompound);
		case 3:
			return parseObjectV3(schematicCompound);
		default:
			throw new UnsupportedOperationException("No implementation for sponge schematic version " + version);
		}
	}
	
	// V1
	protected static final String TAG_VERSION				= "Version";
	protected static final String TAG_METADATA				= "Metadata";
	protected static final String TAG_WIDTH					= "Width";
	protected static final String TAG_HEIGHT				= "Height";
	protected static final String TAG_LENGTH				= "Length";
	protected static final String TAG_OFFSET				= "Offset";
	protected static final String TAG_PALETTE_MAX			= "PaletteMax";
	protected static final String TAG_PALETTE				= "Palette";
	protected static final String TAG_BLOCK_DATA			= "BlockData";
	protected static final String TAG_TILE_ENTITIES			= "TileEntities";
	
	// Metadata tags
	protected static final String TAG_NAME					= "Name";
	protected static final String TAG_AUTHOR 				= "Author";
	protected static final String TAG_DATE					= "Date";
	protected static final String TAG_REQUIRED_MODS			= "RequiredMods";
	
	private CustomObject parseObjectV1(CompoundTag schematicCompound) {
		int dataVersion = AnvilVersion.VERSION_1_13_2.getDataVersion();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		
		CompoundTag paletteTag = schematicCompound.get(TAG_PALETTE);
		ByteArrayTag blockDataTag = (ByteArrayTag) schematicCompound.get(TAG_BLOCK_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockDataTag);
	}
	
	// V2
	protected static final String TAG_DATA_VERSION			= "DataVersion";
	protected static final String TAG_BLOCK_ENTITIES		= "BlockEntities";
	protected static final String TAG_ENTITIES				= "Entities";
	protected static final String TAG_BIOME_PALETTE_MAX		= "BiomePaletteMax";
	protected static final String TAG_BIOME_PALETTE			= "BiomePalette";
	protected static final String TAG_BIOME_DATA			= "BiomeData";
	
	private CustomObject parseObjectV2(CompoundTag schematicCompound) {
		int dataVersion = ((IntTag) schematicCompound.get(TAG_DATA_VERSION)).getValue();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		if (schematicCompound.contains(TAG_METADATA)) {
			// Look for special case with WorldEdit having different offsets for V2
			CompoundTag metadata = schematicCompound.get(TAG_METADATA);
			if (metadata.contains("WEOffsetX")) {
				offset[0] = ((IntTag) metadata.get("WEOffsetX")).getValue();
			}
			if (metadata.contains("WEOffsetY")) {
				offset[1] = ((IntTag) metadata.get("WEOffsetY")).getValue();
			}
			if (metadata.contains("WEOffsetZ")) {
				offset[2] = ((IntTag) metadata.get("WEOffsetZ")).getValue();
			}
		}
		
		CompoundTag paletteTag = schematicCompound.get(TAG_PALETTE);
		ByteArrayTag blockDataTag = (ByteArrayTag) schematicCompound.get(TAG_BLOCK_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockDataTag);
	}
	
	// V3
	protected static final String TAG_BLOCKS				= "Blocks";
	protected static final String TAG_BIOMES				= "Biomes";
	protected static final String TAG_DATA					= "Data";
	
	private CustomObject parseObjectV3(CompoundTag schematicCompound) {
		int dataVersion = ((IntTag) schematicCompound.get(TAG_DATA_VERSION)).getValue();
		
		int width = ((ShortTag) schematicCompound.get(TAG_WIDTH)).getValue() & 0xFFFF;
		int height = ((ShortTag) schematicCompound.get(TAG_HEIGHT)).getValue() & 0xFFFF;
		int length = ((ShortTag) schematicCompound.get(TAG_LENGTH)).getValue() & 0xFFFF;
		
		int[] offset = {0, 0, 0};
		if (schematicCompound.contains(TAG_OFFSET)) {
			offset = ((IntArrayTag) schematicCompound.get(TAG_OFFSET)).getValue();
		}
		
		CompoundTag blockContainerTag = schematicCompound.get(TAG_BLOCK_DATA);
		CompoundTag paletteTag = blockContainerTag.get(TAG_PALETTE);
		ByteArrayTag blockData = (ByteArrayTag) blockContainerTag.get(TAG_DATA);
		
		return makeCustomObject(width, height, length, offset, dataVersion, paletteTag, blockData);
	}

	@Override
	public boolean writeObjectToFile(File file, CustomObject object) throws IOException {
		return false;
	}

	@Override
	public String formatName() {
		return "Sponge Schematic";
	}

	@Override
	public String formatSuffix() {
		return "schem";
	}
	
	private CustomObject makeCustomObject(int width, int height, int length, int[] offset, int dataVersion, CompoundTag paletteTag, ByteArrayTag blocksTag) {
		HashMap<Integer, MinecraftMaterialState> palette = buildPaletteMap(paletteTag, dataVersion);
		ArrayList<Integer> blocksData = decodeVarInt(blocksTag.getValue());
		
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				for (int z = 0; z < length; z++) {
					int index = x + z * width + y * width * length;
					MinecraftMaterialState material = palette.get(blocksData.get(index));
					blocks.add(new Block(x+offset[0], y+offset[1], z+offset[2], material));
				}
			}
		}
		
		return new CustomObject(blocks.toArray(new Block[blocks.size()]));
	}
	
	private ArrayList<Integer> decodeVarInt(byte[] varIntBytes) {
		ArrayList<Integer> ints = new ArrayList<Integer>();
		
        int i = 0;
        int value = 0;
        int varint_length = 0;
        while (i < varIntBytes.length) {
            value = 0;
            varint_length = 0;

            while (true) {
                value |= (varIntBytes[i] & 127) << (varint_length++ * 7);
                if (varint_length > 5) {
                    throw new RuntimeException("VarInt too big (probably corrupted data)");
                }
                if ((varIntBytes[i] & 128) != 128) {
                    i++;
                    break;
                }
                i++;
            }
            
            ints.add(value);
        }
        
        return ints;
	}
	
	private HashMap<Integer, MinecraftMaterialState> buildPaletteMap(CompoundTag paletteTag, int dataVersion) {
		HashMap<Integer, MinecraftMaterialState> palette = new HashMap<Integer, MinecraftMaterialState>();
		
		MinecraftMaterialProfile minecratMaterialProfile = (MinecraftMaterialProfile) MaterialRegistry.getProfile("minecraft");
		AnvilVersion anvilVersion = AnvilVersion.getVersionByDataVersion(dataVersion);
		FlattenedLookup flattenedLookup = minecratMaterialProfile.getFlattenedLookup(anvilVersion.getMaterialVersionName());
		
		paletteTag.forEach(tag -> {
			palette.put((Integer) tag.getValue(), lookupMaterial(tag.getName(), flattenedLookup));
		});
		
		return palette;
	}
	
	private MinecraftMaterialState lookupMaterial(String materialKey, FlattenedLookup flattenedLookup) {
		String id = materialKey;
		ArrayList<Property> properties = new ArrayList<Property>();
		
		if (materialKey.contains("[")) {
			id = materialKey.substring(0, materialKey.indexOf("["));
			String[] propertyStrings = materialKey.substring(materialKey.indexOf("[")+1, materialKey.length()-1).split(",");
			for (String ps: propertyStrings) {
				String[] pair = ps.split("=");
				Property p = new Property(pair[0], pair[1]);
				properties.add(p);
			}
		}
		
		return flattenedLookup.getMaterialState(id, new StateProperties(properties));
	}
}
