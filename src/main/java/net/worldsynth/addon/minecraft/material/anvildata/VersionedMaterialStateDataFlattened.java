/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material.anvildata;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.material.StateProperties;

public class VersionedMaterialStateDataFlattened {
	
	private StateProperties defaultProperties;
	private CompoundTag defaultTileEntity;
	private CompoundTag defaultTileTick;
	private CompoundTag defaultLiquidTick;
	
	private boolean inheritanceBuilt;
	private TreeMap<String, AnvilDataFlattened> anvilData;
	
	public VersionedMaterialStateDataFlattened(TreeMap<String, AnvilDataFlattened> anvilData, StateProperties defaultProperties, CompoundTag defaultTileEntity, CompoundTag defaultTileTick, CompoundTag defaultLiquidTick) {
		this.anvilData = anvilData;
		
		this.defaultProperties = defaultProperties;
		this.defaultTileEntity = defaultTileEntity;
		this.defaultTileTick = defaultTileTick;
		this.defaultLiquidTick = defaultLiquidTick;
	}
	
	/**
	 * Creates a mutated version of the parent instance with the additions and changes from the overriding data.
	 * 
	 * @param parent
	 * @param overridingData
	 */
	@SuppressWarnings("unchecked")
	public VersionedMaterialStateDataFlattened(VersionedMaterialStateDataFlattened parent, Map<String, AnvilDataFlattened> overridingData) {
		this.anvilData = (TreeMap<String, AnvilDataFlattened>) parent.anvilData.clone();
		
		this.defaultProperties = parent.defaultProperties;
		this.defaultTileEntity = parent.defaultTileEntity;
		this.defaultTileTick = parent.defaultTileTick;
		this.defaultLiquidTick = parent.defaultLiquidTick;
		
		overridingData.forEach((version, data) -> {
			anvilData.put(version, data);
		});
	}
	
	/**
	 * Creates a mutated version of the parent instance where tile entity, tile tick and liquid tick is replaced for all versions
	 * @param parent
	 * @param tileEntity
	 * @param tileTick
	 * @param liquidTick
	 */
	public VersionedMaterialStateDataFlattened(VersionedMaterialStateDataFlattened parent, CompoundTag tileEntity, CompoundTag tileTick, CompoundTag liquidTick) {
		anvilData = new TreeMap<String, AnvilDataFlattened>();
		
		this.defaultProperties = parent.defaultProperties;
		this.defaultTileEntity = tileEntity;
		this.defaultTileTick = tileTick;
		this.defaultLiquidTick = liquidTick;
		
		parent.anvilData.forEach((version, data) -> {
			anvilData.put(version, new AnvilDataFlattened(data.id, data.properties, tileEntity, tileTick, liquidTick));
		});
		
		// Since this is a mutation of existing data, the inheritance should already have been built for the parent
		inheritanceBuilt = true;
	}
	
	public AnvilDataFlattened getAnvilData(String versionKey) {
		if (!inheritanceBuilt) {
			throw new IllegalStateException("Inheritance isn't built");
		}
		return anvilData.floorEntry(versionKey).getValue();
	}
	
	public void buildInheritedAnvilData(VersionedMaterialDataFlattened parentMaterialData) {
		if (inheritanceBuilt) {
			throw new IllegalStateException("Inheritance is already built");
		}
		AnvilDataFlattened defaultData = new AnvilDataFlattened(null, defaultProperties, defaultTileEntity, defaultTileTick, defaultLiquidTick);
		
		//Inherit the existence knowledge of versions from parent material
		for (Entry<String, AnvilDataFlattened> entry: parentMaterialData.getAnvilDataMap().entrySet()) {
			if (!anvilData.containsKey(entry.getKey())) {
				anvilData.put(entry.getKey(), new AnvilDataFlattened(null, null, null, null, null));
			}
		}
		
		if (anvilData.floorEntry("1.13") == null) {
			anvilData.put("1.13", defaultData);
		}
		
		for (Entry<String, AnvilDataFlattened> entry: anvilData.entrySet()) {
			entry.setValue(createInheritedData(parentMaterialData.getAnvilData(entry.getKey()), defaultData, entry.getValue()));
			defaultData = entry.getValue();
		}
		
		inheritanceBuilt = true;
	}
	
	private boolean inheritTileEntityFromState = false;
	private boolean inheritTileTickFromState = false;
	private boolean inheritLiquidTickFromState = false;
	private boolean inheritFlattenedIdFromState = false;
	
	private AnvilDataFlattened createInheritedData(AnvilDataFlattened materialParent, AnvilDataFlattened stateParent, AnvilDataFlattened childDiff) {
		//Tile entity inheritance
		CompoundTag tileEntity = childDiff.getTileEntity();
		if (tileEntity == null) {
			//When there is no value for tile entity, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritTileEntityFromState) {
				//If tile entity has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				tileEntity = stateParent.getTileEntity();
			}
			else {
				//If tile entity has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				tileEntity = materialParent.getTileEntity();
			}
		}
		else {
			//When there is a value for tile entity, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of tile entity after this should be done from parent state data.
			inheritTileEntityFromState = true;
		}
		
		//Tile tick inheritance
		CompoundTag tileTick = childDiff.getTileTick();
		if (tileTick == null) {
			//When there is no value for tile tick, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritTileTickFromState) {
				//If tile tick has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				tileTick = stateParent.getTileTick();
			}
			else {
				//If tile tick has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				tileTick = materialParent.getTileTick();
			}
		}
		else {
			//When there is a value for tile tick, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of tile tick after this should be done from parent state data.
			inheritTileTickFromState = true;
		}
		
		//Liquid tick inheritance
		CompoundTag liquidTick = childDiff.getLiquidTick();
		if (liquidTick == null) {
			//When there is no value for liquid tick, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritLiquidTickFromState) {
				//If liquid tick has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				liquidTick = stateParent.getLiquidTick();
			}
			else {
				//If liquid tick has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				liquidTick = materialParent.getLiquidTick();
			}
		}
		else {
			//When there is a value for liquid tick, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of liquid tick after this should be done from parent state data.
			inheritLiquidTickFromState = true;
		}
		
		//Flattened id inheritance
		String id = ((AnvilDataFlattened) childDiff).getId();
		if (id == null) {
			//When there is no value for flattened id, it has not been assigned in the current state data version and should therefore be inherited.
			if (inheritFlattenedIdFromState) {
				//If flattened id has at some previous point been assigned in a state data version, then it should be inherit from the parent state data version.
				id = ((AnvilDataFlattened) stateParent).getId();
			}
			else {
				//If flattened id has never previously been assigned in a state data version, then it should be inherited from the parent material data version.
				id = ((AnvilDataFlattened) materialParent).getId();
			}
		}
		else {
			//When there is a value for flattened id, it has been assigned in the current state data version and should not be inherited.
			//Further inheritance of flattened id after this should be done from parent state data.
			inheritFlattenedIdFromState = true;
		}
		
		//Properties inheritance
		StateProperties properties = ((AnvilDataFlattened) childDiff).getProperties();
		if (properties == null) {
			//When there is no value for properties, it has not been assigned in the current state data version and should therefore be inherited.
			//Properties values are always inherited from the previous state data version.
			properties = ((AnvilDataFlattened) stateParent).getProperties();
		}
		
		return new AnvilDataFlattened(id, properties, tileEntity, tileTick, liquidTick);
	}
}
