/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.steveice10.opennbt.NBTIO;
import com.github.steveice10.opennbt.SNBTIO;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialDataNumeric;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataNumeric;
import net.worldsynth.material.MaterialBuilder;

public class MinecraftMaterialBuilder extends MaterialBuilder<MinecraftMaterial, MinecraftMaterialState> {
	protected CompoundTag tileEntity;
	protected CompoundTag tileTick;
	protected CompoundTag liquidTick;
	
	protected int lightLevel = 0;
	protected int lightDampening = 15;
	
	protected TreeMap<String, AnvilDataNumeric> anvilDataNumeric = new TreeMap<String, AnvilDataNumeric>(MinecraftMaterialProfile.anvilVersionKeyComarator);
	protected TreeMap<String, AnvilDataFlattened> anvilDataFlattened = new TreeMap<String, AnvilDataFlattened>(MinecraftMaterialProfile.anvilVersionKeyComarator);
	
	public MinecraftMaterialBuilder(String idName, String displayName) {
		super(idName, displayName);
	}
	
	public MinecraftMaterialBuilder(String idName, JsonNode node, File materialsFile) {
		super(idName, node, materialsFile);

		if (node.has("tileEntity")) {
			File tileEntityFile = new File(materialsFile.getParent(), node.get("tileEntity").asText());
			if (tileEntityFile != null && tileEntityFile.exists()) {
				try {
					tileEntity = readNbt(tileEntityFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (node.has("tileTick")) {
			File tileTickFile = new File(materialsFile.getParent(), node.get("tileTick").asText());
			if (tileTickFile != null && tileTickFile.exists()) {
				try {
					tileTick = readNbt(tileTickFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (node.has("liquidTick")) {
			File liquidTickFile = new File(materialsFile.getParent(), node.get("liquidTick").asText());
			if (liquidTickFile != null && liquidTickFile.exists()) {
				try {
					liquidTick = readNbt(liquidTickFile);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		if (node.has("lightLevel")) {
			lightLevel = node.get("lightLevel").asInt();
		}
		
		if (node.has("lightDampening")) {
			lightDampening = node.get("lightDampening").asInt();
		}
		
		//Anvil data
		node.fields().forEachRemaining(t -> {
			if (t.getKey().matches("anvil_1\\.(\\d+\\.?)+")) {
				String versionKey = t.getKey().substring(6);
				if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
					addNumericAnvilData(versionKey, new AnvilDataNumeric(t.getValue(), materialsFile));
				}
				else {
					addFlattenedAnvilData(versionKey, new AnvilDataFlattened(t.getValue(), materialsFile));
				}
			}
		});
	}
	
	private CompoundTag readNbt(File nbtFile) throws IOException {
		if (nbtFile.getName().endsWith(".snbt")) {
			return SNBTIO.readFile(nbtFile);
		}
		else if (nbtFile.getName().endsWith(".nbt")) {
			return NBTIO.readFile(nbtFile);
		}
		return null;
	}
	
	@Override
	protected void parseMaterialStates(JsonNode node, File materialsFile) {
		Iterator<JsonNode> iterator = node.iterator();
		while (iterator.hasNext()) {
			new MinecraftMaterialStateBuilder(iterator.next(), materialsFile).createMaterialState();
		}
	}
	
	public MinecraftMaterialBuilder tileEntity(CompoundTag tileEntity) {
		this.tileEntity = tileEntity;
		return this;
	}
	
	public MinecraftMaterialBuilder tileTick(CompoundTag tileTick) {
		this.tileTick = tileTick;
		return this;
	}
	
	public MinecraftMaterialBuilder liquidTick(CompoundTag liquidTick) {
		this.liquidTick = liquidTick;
		return this;
	}
	
	public MinecraftMaterialBuilder lightLevel(int lightLevel) {
		this.lightLevel = lightLevel;
		return this;
	}
	
	public MinecraftMaterialBuilder lightDampening(int lightDampening) {
		this.lightDampening = lightDampening;
		return this;
	}
	
	public MinecraftMaterialBuilder addNumericAnvilData(String versionKey, AnvilDataNumeric anvilData) {
		this.anvilDataNumeric.put(versionKey, anvilData);
		return this;
	}
	
	public MinecraftMaterialBuilder addFlattenedAnvilData(String versionKey, AnvilDataFlattened anvilData) {
		this.anvilDataFlattened.put(versionKey, anvilData);
		return this;
	}

	@Override
	public MinecraftMaterial createMaterial() {
		extrapolateStates(isDefault -> {
			return new MinecraftMaterialStateBuilder(isDefault);
		});
		
		VersionedMaterialDataNumeric versionedDataNumeric = new VersionedMaterialDataNumeric(anvilDataNumeric, -1, (byte) 0, tileEntity, tileTick, liquidTick);
		VersionedMaterialDataFlattened versionedDataFlattened = new VersionedMaterialDataFlattened(anvilDataFlattened, idName, tileEntity, tileTick, liquidTick);
		return new MinecraftMaterial(idName, displayName, color, texture, tags, isAir, properties, states, versionedDataNumeric, versionedDataFlattened, lightLevel, lightDampening);
	}
	
	////////////////////////////
	// Material State Builder //
	////////////////////////////
	
	public class MinecraftMaterialStateBuilder extends MaterialStateBuilder {
		protected CompoundTag tileEntity;
		protected CompoundTag tileTick;
		protected CompoundTag liquidTick;
		
		protected Integer lightLevel;
		protected Integer lightDampening;
		
		protected TreeMap<String, AnvilDataNumeric> anvilDataNumeric = new TreeMap<String, AnvilDataNumeric>(MinecraftMaterialProfile.anvilVersionKeyComarator);
		protected TreeMap<String, AnvilDataFlattened> anvilDataFlattened = new TreeMap<String, AnvilDataFlattened>(MinecraftMaterialProfile.anvilVersionKeyComarator);
		
		public MinecraftMaterialStateBuilder(boolean isDefault) {
			super(isDefault);
		}
		
		public MinecraftMaterialStateBuilder(JsonNode node, File materialsFile) {
			super(node, materialsFile);
			
			if (node.has("tileEntity")) {
				File tileEntityFile = new File(materialsFile.getParent(), node.get("tileEntity").asText());
				if (tileEntityFile != null && tileEntityFile.exists()) {
					try {
						tileEntity = readNbt(tileEntityFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			if (node.has("tileTick")) {
				File tileTickFile = new File(materialsFile.getParent(), node.get("tileTick").asText());
				if (tileTickFile != null && tileTickFile.exists()) {
					try {
						tileTick = readNbt(tileTickFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			if (node.has("liquidTick")) {
				File liquidTickFile = new File(materialsFile.getParent(), node.get("liquidTick").asText());
				if (liquidTickFile != null && liquidTickFile.exists()) {
					try {
						liquidTick = readNbt(liquidTickFile);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			
			if (node.has("lightLevel")) {
				lightLevel = node.get("lightLevel").asInt();
			}
			
			if (node.has("lightDampening")) {
				lightDampening = node.get("lightDampening").asInt();
			}
			
			//Anvil data
			node.fields().forEachRemaining(t -> {
				if (t.getKey().matches("anvil_1\\.(\\d+\\.?)+")) {
					String versionKey = t.getKey().substring(6);
					if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
						addNumericAnvilData(versionKey, new AnvilDataNumeric(t.getValue(), materialsFile));
					}
					else {
						addFlattenedAnvilData(versionKey, new AnvilDataFlattened(t.getValue(), materialsFile));
					}
				}
			});
		}
		
		public MinecraftMaterialStateBuilder tileEntity(CompoundTag tileEntity) {
			this.tileEntity = tileEntity;
			return this;
		}
		
		public MinecraftMaterialStateBuilder tileTick(CompoundTag tileTick) {
			this.tileTick = tileTick;
			return this;
		}
		
		public MinecraftMaterialStateBuilder liquidTick(CompoundTag liquidTick) {
			this.liquidTick = liquidTick;
			return this;
		}
		
		public MinecraftMaterialStateBuilder lightLevel(int lightLevel) {
			this.lightLevel = lightLevel;
			return this;
		}
		
		public MinecraftMaterialStateBuilder lightDampening(int lightDampening) {
			this.lightDampening = lightDampening;
			return this;
		}
		
		public MinecraftMaterialStateBuilder addNumericAnvilData(String versionKey, AnvilDataNumeric anvilData) {
			this.anvilDataNumeric.put(versionKey, anvilData);
			return this;
		}
		
		public MinecraftMaterialStateBuilder addFlattenedAnvilData(String versionKey, AnvilDataFlattened anvilData) {
			this.anvilDataFlattened.put(versionKey, anvilData);
			return this;
		}
		
		@Override
		public MinecraftMaterialState createMaterialState() {
			VersionedMaterialStateDataNumeric versionedDataNumeric = new VersionedMaterialStateDataNumeric(anvilDataNumeric, -1, (byte) 0, tileEntity, tileTick, liquidTick);
			VersionedMaterialStateDataFlattened versionedDataFlattened = new VersionedMaterialStateDataFlattened(anvilDataFlattened, properties, tileEntity, tileTick, liquidTick);
			
			MinecraftMaterialState state = new MinecraftMaterialState(properties, isDefault, displayName, color, texture, versionedDataNumeric, versionedDataFlattened, lightLevel, lightDampening);
			addState(state);
			return state;
		}
	}
}
