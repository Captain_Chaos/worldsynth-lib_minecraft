/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.material;

import com.github.steveice10.opennbt.tag.builtin.CompoundTag;

import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.AnvilDataNumeric;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataFlattened;
import net.worldsynth.addon.minecraft.material.anvildata.VersionedMaterialStateDataNumeric;
import net.worldsynth.color.WsColor;
import net.worldsynth.material.MaterialState;
import net.worldsynth.material.StateProperties;
import net.worldsynth.material.Texture;

public class MinecraftMaterialState extends MaterialState<MinecraftMaterial, MinecraftMaterialState> {
	
	protected VersionedMaterialStateDataNumeric versionedDataNumeric;
	protected VersionedMaterialStateDataFlattened versionedDataFlattened;
	
	protected Integer lightLevel;
	protected Integer lightDampening;
	
	protected MinecraftMaterialState(
			StateProperties properties,
			boolean isDefault,
			String displayName,
			WsColor color,
			Texture texture,
			VersionedMaterialStateDataNumeric versionedDataNumeric,
			VersionedMaterialStateDataFlattened versionedDataFlattened,
			Integer lightlevel,
			Integer lightDampening) {
		super(properties, isDefault, displayName, color, texture);
		
		this.versionedDataNumeric = versionedDataNumeric;
		this.versionedDataFlattened = versionedDataFlattened;
		
		this.lightLevel = lightlevel;
		this.lightDampening = lightDampening;
	}
	
	private String lastAcessedNumericAnvilDataVersionKey;
	private AnvilDataNumeric lastAcessedNumericAnvilData;
	public AnvilDataNumeric getNumericAnvilData(String versionKey) {
		if (versionKey.equals(lastAcessedNumericAnvilDataVersionKey)) {
			return lastAcessedNumericAnvilData;
		}
		
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") >= 0) {
			throw new IllegalArgumentException("Cannot get numeric anvil data for a versions newer than or equal to 1.13");
		}
		lastAcessedNumericAnvilDataVersionKey = versionKey;
		lastAcessedNumericAnvilData = versionedDataNumeric.getAnvilData(versionKey);
		return lastAcessedNumericAnvilData;
	}
	
	private String lastAcessedFlattenedAnvilDataVersionKey;
	private AnvilDataFlattened lastAcessedFlattenedAnvilData;
	public AnvilDataFlattened getFlattenedAnvilData(String versionKey) {
		if (versionKey.equals(lastAcessedFlattenedAnvilDataVersionKey)) {
			return lastAcessedFlattenedAnvilData;
		}
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
			throw new IllegalArgumentException("Cannot get flattened anvil data for a versions older than 1.13");
		}
		lastAcessedFlattenedAnvilDataVersionKey = versionKey;
		lastAcessedFlattenedAnvilData = versionedDataFlattened.getAnvilData(versionKey);
		return lastAcessedFlattenedAnvilData;
	}
	
	public int getNumericAnvilId(String versionKey) {
		AnvilDataNumeric numericAnvilData = getNumericAnvilData(versionKey);
		if (numericAnvilData != null) {
			return numericAnvilData.getId();
		}
		else return 0;
	}
	
	public byte getNumericAnvilMeta(String versionKey) {
		AnvilDataNumeric numericAnvilData = getNumericAnvilData(versionKey);
		if (numericAnvilData != null) {
			return numericAnvilData.getMeta();
		}
		else return 0;
	}
	
	public String getFlattenedAnvilId(String versionKey) {
		AnvilDataFlattened flattenedAnvilData = getFlattenedAnvilData(versionKey);
		if (flattenedAnvilData != null) {
			return flattenedAnvilData.getId();
		}
		return null;
	}
	
	public StateProperties getFlattenedAnvilProperties(String versionKey) {
		AnvilDataFlattened flattenedAnvilData = getFlattenedAnvilData(versionKey);
		if (flattenedAnvilData != null) {
			return flattenedAnvilData.getProperties();
		}
		return null;
	}
	
	private String lastAcessedTileEntityVersionKey;
	private CompoundTag lastAcessedTileEntity;
	public CompoundTag getTileEntity(String versionKey) {
		if (versionKey.equals(lastAcessedTileEntityVersionKey)) {
			return lastAcessedTileEntity;
		}
		
		lastAcessedTileEntityVersionKey = versionKey;
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
			lastAcessedTileEntity = getNumericAnvilData(versionKey).getTileEntity();
		}
		else {
			lastAcessedTileEntity = getFlattenedAnvilData(versionKey).getTileEntity();
		}
		
		return lastAcessedTileEntity;
	}
	
	private String lastAcessedTileTickVersionKey;
	private CompoundTag lastAcessedTileTick;
	public CompoundTag getTileTick(String versionKey) {
		if (versionKey.equals(lastAcessedTileTickVersionKey)) {
			return lastAcessedTileTick;
		}
		
		lastAcessedTileTickVersionKey = versionKey;
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
			lastAcessedTileTick = getNumericAnvilData(versionKey).getTileTick();
		}
		else {
			lastAcessedTileTick = getFlattenedAnvilData(versionKey).getTileTick();
		}
		
		return lastAcessedTileTick;
	}
	
	private String lastAcessedLiquidTickVersionKey;
	private CompoundTag lastAcessedLiquidTick;
	public CompoundTag getLiquidTick(String versionKey) {
		if (versionKey.equals(lastAcessedLiquidTickVersionKey)) {
			return lastAcessedLiquidTick;
		}
		
		lastAcessedLiquidTickVersionKey = versionKey;
		if (MinecraftMaterialProfile.anvilVersionKeyComarator.compare(versionKey, "1.13") < 0) {
			lastAcessedLiquidTick = getNumericAnvilData(versionKey).getLiquidTick();
		}
		else {
			lastAcessedLiquidTick = getFlattenedAnvilData(versionKey).getLiquidTick();
		}
		
		return lastAcessedLiquidTick;
	}
	
	public int getLightLevel() {
		if (lightLevel != null) return lightLevel;
		return getMaterial().getLightLevel();
	}
	
	public int getLightDampening() {
		if (lightDampening != null) return lightDampening;
		return getMaterial().getLightDampening();
	}
}
