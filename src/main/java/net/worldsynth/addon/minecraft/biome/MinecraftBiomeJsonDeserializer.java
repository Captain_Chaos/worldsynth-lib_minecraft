/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.biome;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;

import net.worldsynth.biome.BiomeJsonDeserializer;

public class MinecraftBiomeJsonDeserializer extends BiomeJsonDeserializer<MinecraftBiome> {
	private static final long serialVersionUID = 1L;

	public MinecraftBiomeJsonDeserializer() {
		super();
	}
	
	@Override
	public MinecraftBiome deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
		JsonNode node = jp.getCodec().readTree(jp);
		String idName = (String) ctxt.findInjectableValue("idName", null, null);
		File biomesFile = (File) ctxt.findInjectableValue("biomesFile", null, null);
		return new MinecraftBiomeBuilder(idName, node, biomesFile).createBiome();
	}
}
