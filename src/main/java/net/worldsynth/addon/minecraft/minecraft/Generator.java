/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft;

public class Generator {
	
	private final WorldType worldType;
	private final String customWorldTypeName;
	private final SuperflatOptions superflatOptions;
	private final int generatorVersion = 0;
	
	public Generator(WorldType worldType, String customWorldTypeName) {
		this.worldType = worldType;
		this.customWorldTypeName = customWorldTypeName;
		if (worldType.getName() == "flat") {
			this.superflatOptions = worldType.getOptions();
		}
		else {
			this.superflatOptions = null;
		}
	}
	
	public WorldType getWorldType() {
		return worldType;
	}
	
	public String getWorldTypeName() {
		return worldType == WorldType.CUSTOM ? customWorldTypeName : worldType.getName();
	}
	
	public SuperflatOptions getSuperflatOptions() {
		return superflatOptions;
	}
	
	public int getGeneratorVersion() {
		return generatorVersion;
	}
}
