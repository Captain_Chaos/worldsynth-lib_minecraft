/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.function.Predicate;

import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.IntTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.StringTag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;

public abstract class AnvilChunkWriter<C extends SectionedAnvilChunk<?>> {
	
	public final C chunk;
	public final AnvilVersion anvilVersion;
	
	public AnvilChunkWriter(C chunk) {
		this.chunk = chunk;
		anvilVersion = chunk.getAnvilVersion();
	}
	
	public abstract CompoundTag getChunkCompund();
	
	/*=============================================================
	 * 
	 * Tile entities stuff
	 * 
	 *=============================================================*/
	
	protected ListTag createTileEntitiesListTag(String tagName) {
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		int yMin = chunk.getMinY();
		int yMax = chunk.getMaxY();
		
		ListTag tileEntities = new ListTag(tagName);
		for (int x = 0; x < 16; x++) {
			for (int y = yMin; y <= yMax; y++) {
				for (int z = 0; z < 16; z++) {
					MinecraftMaterialState material = chunk.getMaterial(x, y, z);
					
					//Tile entity
					if (material.getTileEntity(anvilVersion.getVersionName()) != null) {
						tileEntities.add(createTileEntityTag(material, chunkX*16+x, y, chunkZ*16+z));
					}
				}
			}
		}
		
		return tileEntities;
	}
	
	protected CompoundTag createTileEntityTag(MinecraftMaterialState material, int x, int y, int z) {
		CompoundTag tileEntity = material.getTileEntity(anvilVersion.getVersionName()).clone();
		if (!tileEntity.contains("id")) {
			if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13_2) >= 0) {
				tileEntity.put(new StringTag("id", material.getFlattenedAnvilId(anvilVersion.getVersionName())));
			}
			else {
				tileEntity.put(new StringTag("id", material.getMaterial().getIdName()));
			}
		}
		tileEntity.put(new IntTag("x", x));
		tileEntity.put(new IntTag("y", y));
		tileEntity.put(new IntTag("z", z));
		if (!tileEntity.contains("keepPacked")) {
			tileEntity.put(new ByteTag("keepPacked", (byte) 0));
		}
		
		return tileEntity;
	}
	
	/*=============================================================
	 * 
	 * Tile ticks stuff
	 * 
	 *=============================================================*/
	
	protected ListTag createTileTicksListTag(String tagName) {
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		int yMin = chunk.getMinY();
		int yMax = chunk.getMaxY();
		
		ListTag tileTicks = new ListTag(tagName);
		for (int x = 0; x < 16; x++) {
			for (int y = yMin; y <= yMax; y++) {
				for (int z = 0; z < 16; z++) {
					MinecraftMaterialState material = chunk.getMaterial(x, y, z);
					
					//Tile tick
					if (material.getTileTick(anvilVersion.getVersionName()) != null) {
						tileTicks.add(createTileTickTag(material, chunkX*16+x, y, chunkZ*16+z));
					}
				}
			}
		}
		
		return tileTicks;
	}
	
	protected CompoundTag createTileTickTag(MinecraftMaterialState material, int x, int y, int z) {
		CompoundTag tileTick = material.getTileTick(anvilVersion.getVersionName()).clone();
		if (!tileTick.contains("i")) {
			if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13_2) >= 0) {
				tileTick.put(new StringTag("i", material.getFlattenedAnvilId(anvilVersion.getVersionName())));
			}
			else {
				tileTick.put(new StringTag("i", material.getMaterial().getIdName()));
			}
		}
		tileTick.put(new IntTag("x", x));
		tileTick.put(new IntTag("y", y));
		tileTick.put(new IntTag("z", z));
		
		return tileTick;
	}
	
	/*=============================================================
	 * 
	 * Liquid ticks stuff
	 * 
	 *=============================================================*/
	
	protected ListTag createLiquidTicksListTag(String tagName) {
		int chunkX = chunk.getChunkX();
		int chunkZ = chunk.getChunkZ();
		int yMin = chunk.getMinY();
		int yMax = chunk.getMaxY();
		
		ListTag liquidTicks = new ListTag(tagName);
		for (int x = 0; x < 16; x++) {
			for (int y = yMin; y <= yMax; y++) {
				for (int z = 0; z < 16; z++) {
					MinecraftMaterialState material = chunk.getMaterial(x, y, z);
					
					//Tile tick
					if (material.getLiquidTick(anvilVersion.getVersionName()) != null) {
						liquidTicks.add(createLiquidTickTag(material, chunkX*16+x, y, chunkZ*16+z));
					}
				}
			}
		}
		
		return liquidTicks;
	}
	
	protected CompoundTag createLiquidTickTag(MinecraftMaterialState material, int x, int y, int z) {
		CompoundTag liquidTick = material.getLiquidTick(anvilVersion.getVersionName()).clone();
		if (!liquidTick.contains("i")) {
			if (anvilVersion.compareTo(AnvilVersion.VERSION_1_13_2) >= 0) {
				liquidTick.put(new StringTag("i", material.getFlattenedAnvilId(anvilVersion.getVersionName())));
			}
			else {
				liquidTick.put(new StringTag("i", material.getMaterial().getIdName()));
			}
		}
		liquidTick.put(new IntTag("x", x));
		liquidTick.put(new IntTag("y", y));
		liquidTick.put(new IntTag("z", z));
		
		return liquidTick;
	}
	
	/*=============================================================
	 * 
	 * Heightmap creation stuff
	 * 
	 *=============================================================*/
	
//	protected static Predicate<MinecraftMaterialState> PREDICATE_MOTION_BLOCKING = m -> {
//		return !m.isAir();
//	};
//	
//	protected static Predicate<MinecraftMaterialState> PREDICATE_MOTION_BLOCKING_NO_LEAVES = m -> {
//		if (m.isAir()) return false;
//		if (m.getMaterial().hasTag("minecraft:leaves")) return false;
//		return true;
//	};
//	
//	protected static Predicate<MinecraftMaterialState> PREDICATE_OCEAN_FLOOR = m -> {
//		if (m.isAir()) return false;
//		if (m.getMaterial().hasTag("minecraft:fluid")) return false;
//		return true;
//	};
//	
//	protected static Predicate<MinecraftMaterialState> PREDICATE_OCEAN_FLOOR_WG = m -> {
//		if (m.isAir()) return false;
//		if (m.getMaterial().hasTag("minecraft:fluid")) return false;
//		return true;
//	};
	
	protected static Predicate<MinecraftMaterialState> PREDICATE_WORLD_SURFACE = m -> {
		return !m.isAir();
	};
	
//	protected static Predicate<MinecraftMaterialState> PREDICATE_WORLD_SURFACE_WG = m -> {
//		return !m.isAir();
//	};
	
	protected int[] createHeightmap(C chunk, Predicate<MinecraftMaterialState> isTopMaterial) {
		int[] heightmap = new int[256];
		for (int x = 0; x < 16; x++) {
			for (int z = 0; z < 16; z++) {
				for (int y = chunk.getMaxY(); y >= chunk.getMinY(); y--) {
					MinecraftMaterialState material = chunk.getMaterial(x, y, z);
					if (isTopMaterial.test(material)) {
						heightmap[x << 4 | z] = y+1;
						break;
					}
				}
			}
		}
		
		return heightmap;
	}
}
