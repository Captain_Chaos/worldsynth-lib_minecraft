/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import java.util.List;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.ByteTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.WastefulBitArrayUtil;

public class ChunkWriter116 extends ChunkWriter115 {

	public ChunkWriter116(AnvilChunk115 chunk) {
		super(chunk);
	}
	
	@Override
	protected CompoundTag chunkSectionToNbt(AnvilChunkSection112 chunkSection, List<Short> sectionLightsList) {
		CompoundTag sectionCompoundTag = new CompoundTag(ChunkLoader115.TAG_SECTION);
		
		sectionCompoundTag.put(new ByteTag(ChunkLoader115.TAG_Y, (byte) chunkSection.getSectionY()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader115.TAG_BLOCKLIGHT, chunkSection.getBlockLightsArray()));
		sectionCompoundTag.put(new ByteArrayTag(ChunkLoader115.TAG_SKYLIGHT, chunkSection.getSkyLightsArray()));
		
		MinecraftMaterialState[] materials = chunkSection.getMaterialsArray();
		MaterialPalette palette = new MaterialPalette(anvilVersion);
		int[] blockStates = new int[4096];
		
		for (int i = 0; i < 4096; i++) {
			blockStates[i] = palette.getPaletteIndex(materials[i]);
		}
		int bitsPerBlockstateEntry = Math.max((int) Math.ceil(Math.log(palette.getPaletteSize()) / Math.log(2)), 4);
		sectionCompoundTag.put(new LongArrayTag(ChunkLoader115.TAG_BLOCKSTATES, WastefulBitArrayUtil.toLongBitArray(bitsPerBlockstateEntry, blockStates)));
		
		sectionCompoundTag.put(palette.toNbt(ChunkLoader115.TAG_PALETTE));
		
		// Fill in list of light sources if needed
		if (sectionLightsList != null) {
			boolean hasLightSources = false;
			for (int i = 0; i < palette.getPaletteSize(); i++) {
				if (palette.getMaterialState(i).getLightLevel() > 0) {
					hasLightSources = true;
					break;
				}
			}
			
			if (hasLightSources) {
				for (short i = 0; i < 4096; i++) {
					if (chunkSection.getMaterialsArray()[i].getLightLevel() > 0) {
						// Block array index is YZX packed, light index is ZYX packed, translate that shit.
						int x = i & 0xF;
						int y = i >>> 8 & 0xF;
						int z = i >>> 4 & 0xF;
						short zyxPacked = (short) (z << 8 | y << 4 | x);
						sectionLightsList.add(zyxPacked);
					}
				}
			}
		}
		
		return sectionCompoundTag;
	}
}
