/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon.minecraft.minecraft.anvil.chunk;

import com.github.steveice10.opennbt.tag.builtin.ByteArrayTag;
import com.github.steveice10.opennbt.tag.builtin.CompoundTag;
import com.github.steveice10.opennbt.tag.builtin.ListTag;
import com.github.steveice10.opennbt.tag.builtin.LongArrayTag;

import net.worldsynth.addon.minecraft.material.MinecraftMaterial;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialState;
import net.worldsynth.addon.minecraft.minecraft.anvil.AnvilVersion;
import net.worldsynth.addon.minecraft.minecraft.anvil.util.WastefulBitArrayUtil;

public class ChunkReader116 extends ChunkReader115 {

	public ChunkReader116(CompoundTag chunkTag) {
		super(chunkTag);
	}
	
	@Override
	protected AnvilChunkSection112 chunkSectionFromNbt(CompoundTag chunkSectionCompound, AnvilVersion anvilVersion) {
		byte sectionY = (byte) chunkSectionCompound.get(ChunkLoader116.TAG_Y).getValue();
		AnvilChunkSection112 chunkSection = new AnvilChunkSection112(sectionY);
		
		if (chunkSectionCompound.contains(ChunkLoader116.TAG_BLOCKLIGHT)) {
			chunkSection.setBlockLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader116.TAG_BLOCKLIGHT)).getValue());
		}
		
		if (chunkSectionCompound.contains(ChunkLoader116.TAG_SKYLIGHT)) {
			chunkSection.setSkyLightsArray(((ByteArrayTag) chunkSectionCompound.get(ChunkLoader116.TAG_SKYLIGHT)).getValue());
		}
		
		if (chunkSectionCompound.contains(ChunkLoader116.TAG_BLOCKSTATES)) {
			MaterialPalette palette = new MaterialPalette(anvilVersion, (ListTag) chunkSectionCompound.get(ChunkLoader116.TAG_PALETTE));
			long[] blockStatesBitArray = ((LongArrayTag) chunkSectionCompound.get(ChunkLoader116.TAG_BLOCKSTATES)).getValue();
			int bitsPerEntry = (blockStatesBitArray.length * 64) / 4096;
			int[] blockStates = WastefulBitArrayUtil.intArrayFromLongBitArray(bitsPerEntry, 4096, blockStatesBitArray);
			
			MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
			for (int i = 0; i < 4096; i++) {
				materials[i] = palette.getMaterialState(blockStates[i]);
			}
			chunkSection.setMaterialsArray(materials);
		}
		else {
			MinecraftMaterialState[] materials = new MinecraftMaterialState[4096];
			for (int i = 0; i < 4096; i++) {
				materials[i] = MinecraftMaterial.AIR.getDefaultState();
			}
			chunkSection.setMaterialsArray(materials);
		}
		
		return chunkSection;
	}
}
