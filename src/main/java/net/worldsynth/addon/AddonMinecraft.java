/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
package net.worldsynth.addon;

import java.util.ArrayList;
import java.util.List;

import net.worldsynth.WorldSynthDirectoryConfig;
import net.worldsynth.addon.minecraft.MinecraftModuleRegister;
import net.worldsynth.addon.minecraft.biome.MinecraftBiomeProfile;
import net.worldsynth.addon.minecraft.customobject.Bo2CustomObjectFormat;
import net.worldsynth.addon.minecraft.customobject.SchematicCustomobjectFormat;
import net.worldsynth.addon.minecraft.customobject.SpongeSchematicCustomobjectFormat;
import net.worldsynth.addon.minecraft.material.MinecraftMaterialProfile;
import net.worldsynth.biome.BiomeProfile;
import net.worldsynth.customobject.CustomObjectFormat;
import net.worldsynth.material.MaterialProfile;
import net.worldsynth.module.AbstractModuleRegister;
import net.worldsynth.patcher.WorldSynthPatcher;

public class AddonMinecraft implements IAddon {
	
	public static void main(String[] args) {
		WorldSynthPatcher.startPatcher(args, new AddonMinecraft());
	}
	
	@Override
	public void initAddon(WorldSynthDirectoryConfig directoryConfig) {}
	
	@Override
	public List<AbstractModuleRegister> getAddonModuleRegisters() {
		ArrayList<AbstractModuleRegister> moduleRegisters = new ArrayList<AbstractModuleRegister>();
		moduleRegisters.add(new MinecraftModuleRegister());
		return moduleRegisters;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<MaterialProfile> getAddonMaterialProfiles() {
		ArrayList<MaterialProfile> materialProfiles = new ArrayList<MaterialProfile>();
		materialProfiles.add(new MinecraftMaterialProfile());
		return materialProfiles;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<BiomeProfile> getAddonBiomeProfiles() {
		ArrayList<BiomeProfile> biomeProfiles = new ArrayList<BiomeProfile>();
		biomeProfiles.add(new MinecraftBiomeProfile());
		return biomeProfiles;
	}

	@Override
	public List<CustomObjectFormat> getAddonCustomObjectFormats() {
		ArrayList<CustomObjectFormat> objectFormats = new ArrayList<CustomObjectFormat>();
		objectFormats.add(new Bo2CustomObjectFormat());
		objectFormats.add(new SchematicCustomobjectFormat());
		objectFormats.add(new SpongeSchematicCustomobjectFormat());
		return objectFormats;
	}
}
